import { Component, OnInit } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';
import { AngularFireAuth } from 'angularfire2/auth';
import { MainService } from '../services/main.service';

@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.scss']
})
export class NavComponent implements OnInit {
  menuState:boolean = false;
  adminNav:boolean = false;
  constructor(public router: Router,private firebaseAuth:AngularFireAuth,private mainService:MainService) {
    this.router.events.subscribe((event)=>{
      if(event instanceof NavigationEnd){
        this.adminNav = event.urlAfterRedirects.indexOf("/admin")>-1 && event.urlAfterRedirects != "/admin/login";; 
      }
    });
  }

  ngOnInit() {

  }

  toggleMenu(){
    this.menuState = !this.menuState;
  }
  
  setMenu(bool){
    this.menuState = bool;
  }

  logOut() {
    this.firebaseAuth
      .auth
      .signOut();
      this.router.navigate(['/'])
  }

}
