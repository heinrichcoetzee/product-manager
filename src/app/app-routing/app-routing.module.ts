
import { NgModule, Injectable } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes, CanActivate, Router } from '@angular/router';
import { LandingComponent } from '../landing/landing.component';
import { ContactComponent } from '../contact/contact.component';
import { ProductsComponent } from '../products/products.component';
import { FaqComponent } from '../faq/faq.component';
import { AboutComponent } from '../about/about.component';
import { AdminComponent } from '../admin/admin.component';
import { ManageProductsComponent } from '../admin/manage-products/manage-products.component';
import { LoginComponent } from '../admin/login/login.component';
import { ProductInfoComponent } from '../products/product-info/product-info.component';
import { AngularFireAuth } from 'angularfire2/auth';
import { AnalyticsComponent } from '../admin/analytics/analytics.component';
import { OrdersComponent } from '../admin/orders/orders.component';

@Injectable()
export class CanActivateRoute implements CanActivate {
  
  constructor(private router: Router,private afAuth: AngularFireAuth) { 
   
  }
  canActivate(): boolean {
    if(this.afAuth.auth.currentUser == null){
      this.router.navigate(['admin/login'])
    }
    return this.afAuth.auth.currentUser != null;
  }
}

const routes: Routes = [
  { path: '', redirectTo: '/home', pathMatch: 'full' },
  { path: '', component: LandingComponent },
  { path: 'home', component: LandingComponent },
  { path: 'about', component: AboutComponent },
  { path: 'faq', component: FaqComponent },
  { path: 'products', component: ProductsComponent },
  { path: 'category/:id/product/:id', component: ProductInfoComponent },
  { path: 'contactus', component: ContactComponent },
  
  { path: 'admin', component: AdminComponent,children:[
    { path: '', redirectTo: '/login', pathMatch: 'full' },
    { path: 'login', component: LoginComponent },
    { path: 'manageproducts', component: ManageProductsComponent,canActivate:[CanActivateRoute] },
    { path: 'analytics', component: AnalyticsComponent,canActivate:[CanActivateRoute] },
    { path: 'orders', component: OrdersComponent,canActivate:[CanActivateRoute] }
  ] },

];

@NgModule({
  imports: [
      CommonModule,
      [RouterModule.forRoot(routes)]
  ],
  exports: [RouterModule],
  providers: [CanActivateRoute],
  declarations: []
})
export class AppRoutingModule { }