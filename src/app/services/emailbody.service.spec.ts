import { TestBed, inject } from '@angular/core/testing';

import { EmailbodyService } from './emailbody.service';

describe('EmailbodyService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [EmailbodyService]
    });
  });

  it('should be created', inject([EmailbodyService], (service: EmailbodyService) => {
    expect(service).toBeTruthy();
  }));
});
