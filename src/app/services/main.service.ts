import { Injectable } from '@angular/core';

import { HttpClient, HttpHeaders } from '@angular/common/http';

import { AngularFireDatabase } from 'angularfire2/database';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class MainService {
  public catObserve:Observable<any>;
  public categories;
  public tempCategoryID;
  constructor(private http: HttpClient,private db: AngularFireDatabase) {
    this.catObserve = this.db.list('categories').valueChanges();
    this.catObserve.subscribe((val) => {
      this.categories = val;
    });

  }

  sendEmail(data): Promise<any> {

    let url = `cloud function url`

    
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json',
        'Access-Control-Allow-Origin': '*'
      })
    };

    return this.http.post(url, data, httpOptions).toPromise().then(response => response);
    
  }

}
