import { Pipe, PipeTransform } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';


@Pipe({name: 'safeHtmlList'})
export class SafeHtmlListPipe {
  constructor(private sanitizer:DomSanitizer){}

  transform(style) {    
    const lines = style.split('-');
    let newString = "<ul>";
    for(let i=1;i<lines.length;i++){
      newString +='<li>'+ lines[i] + '</li>';
      if(i==lines.length){
        newString+="</ul>"
      }
    }
    return this.sanitizer.bypassSecurityTrustHtml(newString);
    //return this.sanitizer.bypassSecurityTrustStyle(style);
  }
}

@Pipe({name: 'safeHtml'})
export class SafeHtmlPipe {
  constructor(private sanitizer:DomSanitizer){}

  transform(style) {
    return this.sanitizer.bypassSecurityTrustHtml(style);
    //return this.sanitizer.bypassSecurityTrustStyle(style);
  }
}




