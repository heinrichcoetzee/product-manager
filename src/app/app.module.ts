import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { CarouselModule } from 'ngx-bootstrap/carousel';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { AccordionModule } from 'ngx-bootstrap/accordion';
import { AngularFireModule } from 'angularfire2';
import { AngularFireAuthModule } from 'angularfire2/auth';
import {  AngularFireDatabaseModule } from 'angularfire2/database';
import {environment} from '../environments/environment';
import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing/app-routing.module';
import { LandingComponent } from './landing/landing.component';
import { AboutComponent } from './about/about.component';
import { FaqComponent } from './faq/faq.component';
import { ContactComponent } from './contact/contact.component';
import { ProductsComponent } from './products/products.component';
import { NavComponent } from './nav/nav.component';
import { AdminComponent } from './admin/admin.component';
import { ManageProductsComponent } from './admin/manage-products/manage-products.component';
import { MainService } from './services/main.service';
import { LoginComponent } from './admin/login/login.component';
import { ProductInfoComponent } from './products/product-info/product-info.component';
import { NgxGalleryModule } from 'ngx-gallery';
import { HttpClientModule } from '@angular/common/http';
import { AngularFireStorageModule } from 'angularfire2/storage';
import { ButtonLoaderComponent } from './button-loader/button-loader.component';
import { Ng2ImgMaxModule } from 'ng2-img-max';
import { AnalyticsComponent } from './admin/analytics/analytics.component';
import { OrdersComponent } from './admin/orders/orders.component';
import { SafeHtmlPipe, SafeHtmlListPipe } from './services/safe-html.pipe';
import { EmailbodyService } from './services/emailbody.service';

@NgModule({
  declarations: [
    AppComponent,
    LandingComponent,
    AboutComponent,
    FaqComponent,
    ContactComponent,
    ProductsComponent,
    NavComponent,
    AdminComponent,
    ManageProductsComponent,
    LoginComponent,
    ProductInfoComponent,
    ButtonLoaderComponent,
    AnalyticsComponent,
    OrdersComponent,
    SafeHtmlPipe,
    SafeHtmlListPipe
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    AppRoutingModule,
    AngularFireModule.initializeApp(environment.config),
    AngularFireAuthModule,
    AngularFireDatabaseModule,
    AngularFireStorageModule,
    CarouselModule.forRoot(),
    AccordionModule.forRoot(),
    BsDropdownModule.forRoot(),
    NgxGalleryModule,
    Ng2ImgMaxModule
  ],
  providers: [MainService,EmailbodyService],
  bootstrap: [AppComponent]
})
export class AppModule { }
