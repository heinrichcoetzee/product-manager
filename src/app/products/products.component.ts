import { Component, OnInit } from '@angular/core';
import { MainService } from '../services/main.service';
import { AngularFireDatabase } from 'angularfire2/database';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.scss']
})
export class ProductsComponent implements OnInit {
  loading: boolean = false;
  categories: Array<any>;
  selectedCategory: any;
  products: Array<any>;
  constructor(public db: AngularFireDatabase, private mainService: MainService) {
    this.categories = [];
    this.products = [];

  }

  ngOnInit() {
    this.loading = true;
      this.mainService.catObserve.subscribe((val) => {
        this.categories = val;
        if(this.mainService.tempCategoryID){
          let find = this.categories.find(i=>i._id==this.mainService.tempCategoryID);
          this.setProducts(find);
        }else{
          this.setProducts(this.categories[0]);
        }
        this.loading = false;
      });


  
  }

  setProducts(cat) {
    this.selectedCategory = cat;
    if(cat.products != undefined){
      this.products = cat.products.sort((a, b) => this.compare(a, b));
    }else{
      this.products = [];
    }

  };

  compare(a, b) {
    if (a.productNo > b.productNo) {
      return 1;
    } else if (a.productNo < b.productNo) {
      return -1;
    }
    return 0;
  }


}
