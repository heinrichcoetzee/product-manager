import { Component, OnInit, Pipe } from '@angular/core';
import { Router } from '@angular/router';
import { AngularFireDatabase, AngularFireList } from 'angularfire2/database';
import { Observable } from 'rxjs';

import { MainService } from '../../services/main.service';
import { EmailbodyService } from '../../services/emailbody.service';

interface IRequest {
  _id:string,
  name: string,
  email: string,
  phone: string,
  product: string,
  category: string,
  read:boolean
}

@Component({
  selector: 'app-product-info',
  templateUrl: './product-info.component.html',
  styleUrls: ['./product-info.component.scss']
})
export class ProductInfoComponent implements OnInit {
  productId;
  categoryId;
  product;
  catRef: Observable<any>;
  requestRef: AngularFireList<any>;
  selectedImage;
  selectedCategory;
  showForm:boolean;
  request:IRequest = {
    _id:"",
    name: "",
    email: "",
    phone: "",
    product: "",
    category: "",
    read:false
  };
  successMessage:string = "";
  hasSpecs:boolean = false;

  constructor(private router: Router, private db: AngularFireDatabase, private mainService: MainService,private emailBody:EmailbodyService) {
    this.showForm = false;
    this.productId = this.router.url.split('/')[4];
    this.categoryId = this.router.url.split('/')[2];

    this.catRef = this.db.list('categories').valueChanges();
    this.requestRef = this.db.list('orders');

   
  }
  ngOnInit() {
    this.catRef.subscribe(i => {
      const cat = i.find(f => f._id == this.categoryId);
      this.selectedCategory = cat;
      this.product = cat.products.find(f => f._id == this.productId);
      this.chooseImage(this.product.mainImageUrl);
      this.checkSpecs()
      this.request = {
        _id:this.db.createPushId(),
        name: "",
        email: "",
        phone: "",
        product: this.product.name,
        category: this.selectedCategory.name,
        read:false
      };
    });
  }

  checkSpecs(){
    const keys = Object.keys(this.product.specifications);
    for(let key of keys){
      if(this.product.specifications[key] != ""){
        this.hasSpecs = true;
        return;
      }
    }  
  }

  sendRequest() {
    let sendObj = {
      "from":"",
      "to":"",
      "subject":"Order request " + this.request._id,
      "html":this.emailBody.compileEmail(`
        <p>ID: ${this.request._id}</p>
        <p>Name: ${this.request.name}</p>
        <p>Email: ${this.request.email}</p>
        <p>Phone: ${this.request.phone}</p>
        <p>Product: ${this.request.product}</p>
        <p>Category: ${this.request.category}</p>`)
    };

    let sendObjClient = {
      "from":"",
      "to":this.request.email,
      "subject":"Order request Received",
      "html":this.emailBody.compileEmail(`<p>Dear ${this.request.name}</p>
      <p>Thank you for your interest.</p>
      <p>We have received your request and we will be in touch soon.</p>
      <p>Best Regards</p>
      `)
    };




        this.requestRef.push(this.request).then((req)=>{
          this.successMessage = "Request Sent Successfully!";
          //send To Company
          this.mainService.sendEmail(sendObj).then((res)=>{
            console.log("status",res.status)
          },(err)=>{
            console.log("status",err.status)
          });
          
          // Send To client
          this.mainService.sendEmail(sendObjClient).then((res)=>{
            console.log("status",res.status)
          }).catch((err)=>{
              console.log("status",err.status)
          });

          this.request = {
            _id:this.db.createPushId(),
            name: "",
            email: "",
            phone: "",
            product: this.product.name,
            category: this.selectedCategory.name,
            read:false
          };
        
        });
      
      
    

  }


  chooseImage(value) {
    this.selectedImage = value;
  }
  

}
