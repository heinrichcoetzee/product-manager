import { Component, OnInit } from '@angular/core';
import { MainService } from '../services/main.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-landing',
  templateUrl: './landing.component.html',
  styleUrls: ['./landing.component.scss']
})
export class LandingComponent implements OnInit {

  constructor(public mainService:MainService, private router:Router) { }

  ngOnInit() {
  }


  goToProduct(id){
    this.mainService.tempCategoryID = id; 
    this.router.navigate(['/products']);
  }
  

}
