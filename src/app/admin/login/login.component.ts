import { Component, OnInit } from '@angular/core';
import { AngularFireAuth } from 'angularfire2/auth';
import { Observable } from 'rxjs';
import * as firebase from 'firebase/app';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  loading:boolean;
  email: string;
  password: string;
  user: Observable<firebase.User>;
  errorMessage:string;
  constructor(private firebaseAuth: AngularFireAuth,private router:Router) {
    this.loading = false;
    this.email = "";
    this.password = "";
    this.user = firebaseAuth.authState;
    this.errorMessage = ""
  }

  ngOnInit() {
    // this.logout()
    this.firebaseAuth.authState.subscribe(v=>{
      if(v!= null){
        this.router.navigate(['/admin/manageproducts'])
      }
    });
   
  }

  login() {
    this.errorMessage = "";
    this.loading = true;
    this.firebaseAuth
      .auth
      .signInWithEmailAndPassword(this.email, this.password)
      .then(value => {
        localStorage.setItem('refreshToken',value.refreshToken);
        this.loading = false;
        this.router.navigate(['/admin/manageproducts']);
      })
      .catch(err => {
        this.errorMessage = err.message;
        this.loading = false
        console.log('Something went wrong:', err.message);
      });
  }

  logout() {
    this.firebaseAuth
      .auth
      .signOut();
  }

}
