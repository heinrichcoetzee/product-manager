import { Component, OnInit } from '@angular/core';
import { AngularFireAuth } from 'angularfire2/auth';
import { AngularFireDatabase, AngularFireList } from 'angularfire2/database';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { AngularFireStorage } from 'angularfire2/storage';
import { Ng2ImgMaxService } from 'ng2-img-max';

interface IProduct {
  name: string,
  _id: string,
  shortDescription: string,
  price: number,
  description: string,
  mainImage?: {
    name?:string
  },
  image1?: string,
  image2?: string,
  image3?: string,
  specifications: {
    volume: string,
    power: string,
    voltage: string,
    frequency: string,
    weight: string,
    waterConsumption: string,
    sizeAirOutlet: string,
    outsideDimension: string,
    applicableArea: string
  }
}

@Component({
  selector: 'app-manage-products',
  templateUrl: './manage-products.component.html',
  styleUrls: ['./manage-products.component.scss']
})
export class ManageProductsComponent implements OnInit {
  categories: Observable<any[]>
  selectedCategory: any;
  newProduct:IProduct;
  newCategory;
  existingProduct:IProduct;
  catRef: AngularFireList<any>;
  loading: boolean;
  errorMessage: string = "";
  imageId1;
  imageId2;
  imageId3;
  statusMessage;
  editingProduct:boolean;
  constructor(public firebaseAuth: AngularFireAuth,
    public db: AngularFireDatabase,
    public storage: AngularFireStorage,
    private ng2ImgMaxService: Ng2ImgMaxService
  ) {
    this.loading = true;
    this.editingProduct = false;
    this.catRef = this.db.list('categories');
    this.categories = this.catRef.snapshotChanges().pipe(
      map(changes =>
        changes.map(c => ({ key: c.payload.key, ...c.payload.val() }))
      )
    )
    
  }
  ngOnInit() {
    this.subscribeToCat((cat) => {
      this.setCategory(cat[0]);
      this.loading = false;
    });
  }


  logout() {
    this.firebaseAuth
      .auth
      .signOut();
  }
  
  cancelProduct(){
    this.newProduct = undefined;
  }
  cancelExistingProduct(){
    this.existingProduct = undefined;
    this.editingProduct = false;
  }

  cancelCategory(){
    this.newCategory = undefined;
  }

  editProduct(product:IProduct){
    this.editingProduct = true;
    this.existingProduct = product;
  }


  subscribeToCat(func) {
    this.categories.subscribe(cat => {
      func(cat)
    });
  }


  deleteCat(cat) {
    if (cat.products != undefined) {
      for (let product of cat.products) {
        this.storage.ref(product._id).delete();
        for (let image of product.images) {
          this.storage.ref(image.id).delete();
        }
      }
    }

    this.catRef.remove(cat.key);
  }

  deleteProduct(id) {
    let index = this.selectedCategory.products.findIndex(k => k._id == id);
    this.storage.ref(this.selectedCategory.products[index]._id).delete();
    if (this.selectedCategory.products[index].image1Url) {
      this.storage.ref(this.selectedCategory.products[index].image1Url.id).delete();
    }
    if (this.selectedCategory.products[index].image2Url) {
      this.storage.ref(this.selectedCategory.products[index].image2Url.id).delete();
    }
    if (this.selectedCategory.products[index].image3Url) {
      this.storage.ref(this.selectedCategory.products[index].image3Url.id).delete();
    }

    this.selectedCategory.products.splice(index, 1);
    this.catRef.update(this.selectedCategory.key, this.selectedCategory);
  }

  addCategory() {
    this.newCategory = {
      name: "",
      _id: this.db.createPushId(),
      products: []
    };
  }


  addProduct() {
    this.newProduct = {
      name: "",
      _id: this.db.createPushId(),
      shortDescription: "",
      price: 0,
      description: "",
      specifications:{
        volume:"",
        power:"",
        voltage:"",
        frequency:"",
        weight:"",
        waterConsumption:"",
        sizeAirOutlet:"",
        outsideDimension:"",
        applicableArea:""
      }
    }
  }


  saveProduct(){
    this.loading = true;
   
    this.uploadImages('existingProduct').then(() => {
      this.assignUrls('existingProduct',()=>{
          delete this.existingProduct.mainImage;
          delete this.existingProduct.image1;
          delete this.existingProduct.image2;
          delete this.existingProduct.image3;
    
          let index = this.selectedCategory.products.findIndex(i=>i._id==this.existingProduct._id);
          this.selectedCategory.products[index] = this.existingProduct;
          
          this.catRef.update(this.selectedCategory.key, this.selectedCategory).then(()=>{
            this.editingProduct = false;
            this.existingProduct = undefined;
            this.loading = false;
          });

        });
         
      });



  }

  createProduct() {
    if (this.newProduct.mainImage.name == undefined) {
      this.errorMessage = "Please upload Main Image";
      return;
    }

    this.errorMessage = "";
    this.loading = true
    this.uploadImages('newProduct').then(() => {
            this.assignUrls('newProduct',()=>{
                delete this.newProduct.mainImage;
                delete this.newProduct.image1;
                delete this.newProduct.image2;
                delete this.newProduct.image3;
          
                if (this.selectedCategory.products == undefined) {
                  this.selectedCategory.products = [this.newProduct];
                } else {
                  this.selectedCategory.products.push(this.newProduct);
                }
                
                this.catRef.update(this.selectedCategory.key, this.selectedCategory);
                this.newProduct = undefined;
                this.loading = false;
              });
               
            });
    
  }

  createCategory() {
    if(this.newCategory.name.trim() == ""){
      return;
    }
    this.catRef.push(this.newCategory);
    this.newCategory = undefined;
  }

  uploadError() {
    this.errorMessage = "Image Upload Error";
  }

  async uploadImages(property) {
    if(this[property].mainImage){
    await this.storage.upload(this[property]._id, this[property].mainImage).then(()=>{
    this.statusMessage = "Uploaded Main Image";
    }).catch(() => { this.uploadError() });
    }

    if (this[property].image1) {
      this.imageId1 = this.db.createPushId();
      await this.storage.upload(this.imageId1, this[property].image1).then(()=>{
      this.statusMessage = "Uploaded Image 1";
      }).catch(() => { this.uploadError() })
    }

    if (this[property].image2) {
      this.imageId2 = this.db.createPushId();
      await this.storage.upload(this.imageId2, this[property].image2).then(()=>{
      this.statusMessage = "Uploaded Image 2";
      }).catch(() => { this.uploadError() })
    }

    if (this[property].image3) {
      this.imageId3 = this.db.createPushId();
      await this.storage.upload(this.imageId3, this[property].image3).then(()=>{
      this.statusMessage = "Uploaded Image 3";
      }).catch(() => { this.uploadError() })
    }

  }

  async assignUrls(property,callback) {
        let promiseArray = []
    
    if(this[property].mainImage!= undefined){
    promiseArray.push(
      new Promise((resolve, reject) => {
        this.storage.ref(this[property]._id).getDownloadURL().subscribe(url =>{
          this[property].mainImageUrl = url;
          console.log("assignedUrlMain");
          resolve(url)
        })
      })
    )
    }

      
    if (this.imageId1 != undefined) {
        promiseArray.push(
          new Promise((resolve)=>{
            this.storage.ref(this.imageId1).getDownloadURL().subscribe(url =>{
              this[property].image1Url = { id: this.imageId1, url: url}
              resolve(url);
            }); 
          }))
    }

    if (this.imageId2 != undefined) {
      promiseArray.push(
        new Promise((resolve)=>{
          this.storage.ref(this.imageId2).getDownloadURL().subscribe(url =>{
            this[property].image2Url = { id: this.imageId2, url: url}
            resolve(url);
          }); 
        }))
  }
  if (this.imageId3 != undefined) {
    promiseArray.push(
      new Promise((resolve)=>{
        this.storage.ref(this.imageId3).getDownloadURL().subscribe(url =>{
          this[property].image3Url = { id: this.imageId3, url: url}
          resolve(url);
        }); 
      }))
}

    Promise.all(promiseArray).then(()=>{
      callback();
    });
   
    }


  setCategory(cat) {
    this.cancelProduct();
    this.cancelExistingProduct()
    if (cat) {
      this.selectedCategory = cat;
      if (this.selectedCategory.product && this.selectedCategory.product.length) {
        this.selectedCategory.products.map(product => {
          product.mainImage = this.storage.ref(product._id).getDownloadURL();
          return product;
        });
      }
    }

  }

  getImageUrl(id) {
    return this.storage.ref(id).getDownloadURL();
  }

  selectFiles(event: any, document: string,property: string): void {
    this.errorMessage = "";
    const fileList: FileList = event.target.files;

    if (fileList.length > 0) {
      const file: any = fileList[0];
      this.ng2ImgMaxService.resize([file], 800, 800).subscribe((result)=>{
        this[property][document] = result;
        console.log(this[property]);
      });
      
    }
  }

  selectFile(event: any,property: string): void {
    const fileList: FileList = event.target.files;
    if (fileList.length > 0) {
      const file: any = fileList[0];
      this.ng2ImgMaxService.resize([file], 800, 800).subscribe((result)=>{
        this[property].mainImage = result;
      });
      
    }
  }

  deleteImage(property){
    let index = this.selectedCategory.products.findIndex(k => k._id == this.existingProduct._id);

    if(property == 'mainImageUrl'){
      this.storage.ref(this.existingProduct._id).delete();
    }else{
      this.storage.ref(this.existingProduct[property].id).delete();
    }
    delete this.existingProduct[property];
    this.selectedCategory.products[index] = this.existingProduct;
    this.catRef.update(this.selectedCategory.key, this.selectedCategory).then(()=>{});

  }

}
