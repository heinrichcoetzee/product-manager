import { Component, OnInit } from '@angular/core';
import { AngularFireDatabase, AngularFireList } from 'angularfire2/database';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Component({
  selector: 'app-orders',
  templateUrl: './orders.component.html',
  styleUrls: ['./orders.component.scss']
})
export class OrdersComponent implements OnInit {
  orders:Observable<any>;
  afOrder: AngularFireList<any>;
  constructor(private db:AngularFireDatabase) { }

  ngOnInit() {
    
    this.afOrder = this.db.list('orders')
    this.orders = this.afOrder.snapshotChanges().pipe(
      map(changes =>
        changes.map(c => ({ key: c.payload.key, ...c.payload.val() }))
      )
    );
    
  }

  setRead(bool,item){
    item.read = bool;
    this.afOrder.update(item.key,item);
  }

}
