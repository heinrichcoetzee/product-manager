
### How do I get set up? ###

- Install the latest Angular CLI npm install -g @angular/cli

- Install the Firebase CLI npm install -g firebase-tools

- Run npm install

- Create a new firebase project on https://firebase.google.com/

- Run firebase init

- Setup Database, Functions, Hosting, Storage

- Add the Firebase Variables in the environment and prod environment.

- Run ng serve to kickoff the Angular application that will run on  http://localhost:4200


